#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtGui/QPushButton>
#include <QtGui/QMessageBox>





void MainWindow::populate()
{
    char aTmp[10];
    int index = 0;

    QGridLayout *grid = ui->gridLayout_1;

    for (int row = 0; row < 8; ++row)
    {
            for (int column = 0; column < 10; ++column)
            {
		index = row *10 +column +1;

                aTmp[0]= '0'+index/10;
                aTmp[1]= '0'+index%10;
		aTmp[2]=  0;

		QPushButton *bt = &allBt[index];
               	bt->setText(aTmp);
                bt->setStyleSheet("color: white;background-color: rgb(100,0,255)");
                bt->setFont(QFont("Times", 30));
                bt->setFlat(0);
                bt->setFocusPolicy(Qt::NoFocus);

                connect(bt, SIGNAL(clicked()),this, SLOT(clickedSlot()));
                grid->addWidget(bt, row, column);

            }
        }
}



//=====================================================================================================================
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    populate();
}
//=====================================================================================================================
MainWindow::~MainWindow()
{
    delete ui;
}

//=====================================================================================================================
void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
//=====================================================================================================================

int ul =0;
int ur =0;
int dl =0;
int dr =0;

void MainWindow::clickedSlot()
{
    QPushButton *bt;
    char aTmp[10];

    bt = ((QPushButton*)sender());

    if(bt->text().length()!=1 )
    {
        if(bt->text().toInt()== 1)  ul = 1;
        if(bt->text().toInt()==10)  ur = 1;
        if(bt->text().toInt()==71)  dl = 1;
        if(bt->text().toInt()==80)  dr = 1;

        if( ul && ur && dl && dr) exit(0);

        bt->setStyleSheet("color: white;background-color:rgb(173,255,47)");
        bt->setText(" ");
    }
    else
    {
        bt->setStyleSheet("color: white;background-color: rgb(100,0,255)");
        bt->setText("..");
    }
};
//=====================================================================================================================

