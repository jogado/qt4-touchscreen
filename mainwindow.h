#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui/QPushButton>


namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    void populate();
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QPushButton allBt[81];

protected:
    void changeEvent(QEvent *e);

public slots:
   void clickedSlot();
private:
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H



