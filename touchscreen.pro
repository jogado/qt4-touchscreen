# -------------------------------------------------
# Project created by QtCreator 2014-03-28T12:07:18
# -------------------------------------------------
TARGET = touchscreen
TEMPLATE = app
SOURCES += main.cpp
SOURCES += mainwindow.cpp 
#SOURCES += moc_mainwindow.cpp

LIBS \
    += \
    -lQtScript \
    -lQtScriptTools \
    -lQtXml \
    -lQtGui \
    -lQtCore \
    -lQtWebKit 
#    -lQtUiTools

HEADERS += mainwindow.h
FORMS += mainwindow.ui
